import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class TestLogin(unittest.TestCase):

    def setUp(self):
        # Configuração do WebDriver (certifique-se de ter o WebDriver adequado instalado, como o ChromeDriver)
        self.driver = webdriver.Chrome()
        self.url = "https://www.saucedemo.com/"

    def tearDown(self):
        # Fecha o navegador após cada teste
        self.driver.quit()

    def test_login_with_valid_credentials(self):
        # Caso de Teste 1.1: Tentativa de login com credenciais válidas.
        # Abre a página do SauceDemo
        self.driver.get(self.url)

        # Insere as credenciais válidas
        username = self.driver.find_element("css selector", "input#user-name")
        password = self.driver.find_element("css selector", "input#password")
        login_button = self.driver.find_element("css selector", "input#login-button")

        username.send_keys("standard_user")
        password.send_keys("secret_sauce")
        login_button.click()

        # Aguarda a página carregar
        self.driver.implicitly_wait(5)

        # Verifica se o login foi bem-sucedido usando assertEquals
        self.assertIn("inventory", self.driver.current_url, "CT1.1: Falha ao executar o teste - Login não bem-sucedido.")

if __name__ == "__main__":
    unittest.main()
