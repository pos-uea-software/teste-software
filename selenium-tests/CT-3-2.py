import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from datetime import datetime

class TestRemoveItemFromCart(unittest.TestCase):

    def setUp(self):
        # Configuração do WebDriver (certifique-se de ter o WebDriver adequado instalado, como o ChromeDriver)
        self.driver = webdriver.Chrome()
        self.url = "https://www.saucedemo.com/"

    def tearDown(self):
        # Fecha o navegador após cada teste
        self.driver.quit()

    def test_remove_item_from_cart(self):
        # Caso de Teste 3.2: Testar a remoção de um item do carrinho.
        # Abre a página do SauceDemo
        self.driver.get(self.url)

        # Insere as credenciais válidas
        username = self.driver.find_element("css selector", "input#user-name")
        password = self.driver.find_element("css selector", "input#password")
        login_button = self.driver.find_element("css selector", "input#login-button")

        username.send_keys("standard_user")
        password.send_keys("secret_sauce")
        login_button.click()

        try:
            # Aguarda até que a lista de produtos esteja visível
            WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CLASS_NAME, "inventory_list"))
            )

            # Adiciona um item ao carrinho
            add_to_cart_button = self.driver.find_element("css selector", "button[data-test='add-to-cart-sauce-labs-backpack']")
            add_to_cart_button.click()

            # Clica no ícone do carrinho
            cart_icon = self.driver.find_element("css selector", "a.shopping_cart_link")
            cart_icon.click()

            # Aguarda até que a página do carrinho seja carregada
            WebDriverWait(self.driver, 10).until(
                EC.title_contains("Your Cart")
            )

            # Verifica se o item está no carrinho antes da remoção usando assertEquals
            cart_items_before_removal = self.driver.find_elements("css selector", "div.inventory_item_name")
            self.assertEqual(len(cart_items_before_removal), 1, "CT 3.2: O item não foi adicionado corretamente ao carrinho.")

            # Clica no botão de remoção do item no carrinho
            remove_button = self.driver.find_element("css selector", "button[data-test='remove-sauce-labs-backpack']")
            remove_button.click()

            # Aguarda até que o carrinho seja atualizado
            WebDriverWait(self.driver, 10).until(
                EC.invisibility_of_element_located((By.CSS_SELECTOR, "button[data-test='remove-sauce-labs-backpack']"))
            )

            # Verifica se o item foi removido corretamente do carrinho usando assertEquals
            cart_items_after_removal = self.driver.find_elements("css selector", "div.inventory_item_name")
            self.assertEqual(len(cart_items_after_removal), 0, "CT 3.2: O item não foi removido corretamente do carrinho.")

            # Verifica se o total foi atualizado usando assertEquals
            total_price_element = self.driver.find_element("css selector", "div.summary_total_label")
            actual_total_price = total_price_element.find_element("xpath", "./following-sibling::div").text
            self.assertEqual(actual_total_price, "$0.00", "CT 3.2: O total no carrinho não foi atualizado corretamente após a remoção.")

        except TimeoutException as e:
            # Captura uma captura de tela e imprime a exceção
            timestamp = datetime.now().strftime("%Y%m%d%H%M%S")
            self.driver.save_screenshot(f"timeout_exception_{timestamp}.png")
            print(f"TimeoutException: {e}")

if __name__ == "__main__":
    unittest.main()
