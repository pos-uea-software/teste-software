import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from datetime import datetime
class TestLogout(unittest.TestCase):

    def setUp(self):
        # Configuração do WebDriver (certifique-se de ter o WebDriver adequado instalado, como o ChromeDriver)
        self.driver = webdriver.Chrome()
        self.url = "https://www.saucedemo.com/"

    def tearDown(self):
        # Fecha o navegador após cada teste
        self.driver.quit()

    def test_logout(self):
        # Caso de Teste 4.1: Testar Logout
        # Abre a página do SauceDemo
        self.driver.get(self.url)

        # Insere as credenciais válidas
        username = self.driver.find_element("css selector", "input#user-name")
        password = self.driver.find_element("css selector", "input#password")
        login_button = self.driver.find_element("css selector", "input#login-button")

        username.send_keys("standard_user")
        password.send_keys("secret_sauce")
        login_button.click()

        try:
          # Aguarda até que a lista de produtos esteja visível
          WebDriverWait(self.driver, 15).until(
              EC.visibility_of_element_located((By.CLASS_NAME, "inventory_list"))
          )

          # Clica no menu
          menu_button = self.driver.find_element("css selector", "button#react-burger-menu-btn")
          menu_button.click()

          # Clica no botão "Logout"
          logout_button = self.driver.find_element("css selector", "a#logout_sidebar_link")
          logout_button.click()

          # Aguarda até que a página de login seja carregada
          WebDriverWait(self.driver, 10).until(
              EC.title_contains("Swag Labs")
          )

          # Verifica se a página de login é exibida corretamente usando assertEquals
          login_page_title = self.driver.title
          expected_login_page_title = "Swag Labs"

          self.assertEqual(login_page_title, expected_login_page_title, "CT 4.1: A página de login não foi exibida corretamente após o logout.")
        except TimeoutException as e:
          # Captura uma captura de tela e imprime a exceção
          timestamp = datetime.now().strftime("%Y%m%d%H%M%S")
          self.driver.save_screenshot(f"timeout_exception_{timestamp}.png")
          print(f"TimeoutException: {e}")
if __name__ == "__main__":
    unittest.main()
