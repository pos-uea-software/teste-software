import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from datetime import datetime

class TestCheckoutWithEmptyCart(unittest.TestCase):

    def setUp(self):
        # Configuração do WebDriver (certifique-se de ter o WebDriver adequado instalado, como o ChromeDriver)
        self.driver = webdriver.Chrome()
        self.url = "https://www.saucedemo.com/"

    def tearDown(self):
        # Fecha o navegador após cada teste
        self.driver.quit()

    def test_checkout_with_empty_cart(self):
        # Caso de Teste 3.4: Tentativa de finalizar a compra com o carrinho vazio.
        # Abre a página do SauceDemo
        self.driver.get(self.url)

        # Insere as credenciais válidas
        username = self.driver.find_element("css selector", "input#user-name")
        password = self.driver.find_element("css selector", "input#password")
        login_button = self.driver.find_element("css selector", "input#login-button")

        username.send_keys("standard_user")
        password.send_keys("secret_sauce")
        login_button.click()

        try:

          WebDriverWait(self.driver, 10).until(
              EC.visibility_of_element_located((By.CSS_SELECTOR, "a.shopping_cart_link"))
          )

          # Clica no ícone do carrinho
          cart_icon = self.driver.find_element("css selector", "a.shopping_cart_link")
          cart_icon.click()

        
          # Aguarda até que a página do carrinho seja carregada
          WebDriverWait(self.driver, 10).until(
              EC.title_contains("Your Cart")
          )

          # Clica no botão "Finalizar Compra"
          checkout_button = self.driver.find_element("css selector", "a.checkout_button")
          checkout_button.click()

          # Aguarda até que a mensagem de erro seja exibida
          WebDriverWait(self.driver, 10).until(
              EC.visibility_of_element_located((By.CSS_SELECTOR, "h3[data-test='error']"))
          )

          # Verifica se a mensagem de erro é exibida corretamente usando assertEquals
          error_message = self.driver.find_element("css selector", "h3[data-test='error']").text
          expected_error_message = "Error: Cart is empty"

          self.assertEqual(error_message, expected_error_message, "CT 3.4: A mensagem de erro não foi exibida corretamente para o carrinho vazio.")
        except TimeoutException as e:
          # Captura uma captura de tela e imprime a exceção
          timestamp = datetime.now().strftime("%Y%m%d%H%M%S")
          self.driver.save_screenshot(f"timeout_exception_{timestamp}.png")
          print(f"TimeoutException: {e}")

if __name__ == "__main__":
    unittest.main()
