import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from datetime import datetime

class TestNavigateToCart(unittest.TestCase):

    def setUp(self):
        # Configuração do WebDriver (certifique-se de ter o WebDriver adequado instalado, como o ChromeDriver)
        self.driver = webdriver.Chrome()
        self.url = "https://www.saucedemo.com/"

    def tearDown(self):
        # Fecha o navegador após cada teste
        self.driver.quit()

    def test_navigate_to_cart(self):
        # Caso de Teste 2.3: Testar o botão "Carrinho".
        # Abre a página do SauceDemo
        self.driver.get(self.url)

        # Insere as credenciais válidas
        username = self.driver.find_element("css selector", "input#user-name")
        password = self.driver.find_element("css selector", "input#password")
        login_button = self.driver.find_element("css selector", "input#login-button")

        username.send_keys("standard_user")
        password.send_keys("secret_sauce")
        login_button.click()

        try:
          # Aguarda até que a lista de produtos esteja visível
          WebDriverWait(self.driver, 15).until(
              EC.visibility_of_element_located((By.CLASS_NAME, "inventory_list"))
          )

          # Clique no botão "Carrinho"
          cart_button = self.driver.find_element("css selector", "a.shopping_cart_link")
          cart_button.click()

          # Aguarda até que a página do carrinho seja carregada
          WebDriverWait(self.driver, 15).until(
              EC.title_contains("Your Cart")
          )

          # Verifica se o usuário foi redirecionado para o carrinho usando assertEquals
          page_title = self.driver.title
          self.assertEqual(page_title, "Swag Labs", "CT 2.3: O usuário não foi redirecionado corretamente para o carrinho.")
        except TimeoutException as e:
            # Captura uma captura de tela e imprime a exceção
            timestamp = datetime.now().strftime("%Y%m%d%H%M%S")
            self.driver.save_screenshot(f"timeout_exception_{timestamp}.png")
            print(f"TimeoutException: {e}")

if __name__ == "__main__":
    unittest.main()
