import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class TestLogin(unittest.TestCase):

    def setUp(self):
        # Configuração do WebDriver (certifique-se de ter o WebDriver adequado instalado, como o ChromeDriver)
        self.driver = webdriver.Chrome()
        self.url = "https://www.saucedemo.com/"

    def tearDown(self):
        # Fecha o navegador após cada teste
        self.driver.quit()

    def test_login_without_credentials(self):
        # Caso de Teste 1.4: Testar o login sem inserir usuário e senha.
        # Abre a página do SauceDemo
        self.driver.get(self.url)

        # Clica no botão de login sem inserir usuário e senha
        login_button = self.driver.find_element("css selector", "input#login-button")
        login_button.click()

        # Aguarda a página carregar
        self.driver.implicitly_wait(5)

        # Verifica se a mensagem de erro é exibida usando assertEquals
        error_message = self.driver.find_element("css selector", "h3[data-test='error']").text
        self.assertEqual(error_message, "Epic sadface: Username is required", "CT 1.4: Mensagem de erro diferente do esperado.")

if __name__ == "__main__":
    unittest.main()
