import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class TestLogin(unittest.TestCase):

    def setUp(self):
        # Configuração do WebDriver (certifique-se de ter o WebDriver adequado instalado, como o ChromeDriver)
        self.driver = webdriver.Chrome()
        self.url = "https://www.saucedemo.com/"

    def tearDown(self):
        # Fecha o navegador após cada teste
        self.driver.quit()

    def test_login_with_invalid_password(self):
        # Caso de Teste 1.3: Testar o login com uma senha inválida.
        # Abre a página do SauceDemo
        self.driver.get(self.url)

        # Insere um nome de usuário válido e uma senha inválida
        username = self.driver.find_element("css selector", "input#user-name")
        password = self.driver.find_element("css selector", "input#password")
        login_button = self.driver.find_element("css selector", "input#login-button")

        username.send_keys("standard_user")
        password.send_keys("123456")
        login_button.click()

        # Aguarda a página carregar
        self.driver.implicitly_wait(5)

        # Verifica se a mensagem de erro é exibida usando assertEquals
        error_message = self.driver.find_element("css selector", "h3[data-test='error']").text
        self.assertEqual(error_message, "Epic sadface: Username and password do not match any user in this service", "CT 1.3: Mensagem de erro diferente do esperado.")

if __name__ == "__main__":
    unittest.main()