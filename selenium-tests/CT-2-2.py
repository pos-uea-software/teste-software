import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from datetime import datetime

class TestAddToCart(unittest.TestCase):

    def setUp(self):
        # Configuração do WebDriver (certifique-se de ter o WebDriver adequado instalado, como o ChromeDriver)
        self.driver = webdriver.Chrome()
        self.url = "https://www.saucedemo.com/"

    def tearDown(self):
        # Fecha o navegador após cada teste
        self.driver.quit()

    def test_add_product_to_cart(self):
        # Caso de Teste 2.2: Testar a adição de um produto ao carrinho.
        # Abre a página do SauceDemo
        self.driver.get(self.url)

        # Insere as credenciais válidas
        username = self.driver.find_element("css selector", "input#user-name")
        password = self.driver.find_element("css selector", "input#password")
        login_button = self.driver.find_element("css selector", "input#login-button")

        username.send_keys("standard_user")
        password.send_keys("secret_sauce")
        login_button.click()

        try:
            # Aguarda até que a lista de produtos esteja visível
            WebDriverWait(self.driver, 15).until(
                EC.visibility_of_element_located((By.CLASS_NAME, "inventory_list"))
            )

            # Clica em "Adicionar ao Carrinho" para o primeiro produto "Sauce Labs Backpack"
            add_to_cart_button = WebDriverWait(self.driver, 15).until(
                EC.element_to_be_clickable((By.CSS_SELECTOR, "button[data-id='add-to-cart-sauce-labs-backpack']"))
            )
            self.driver.execute_script("arguments[0].scrollIntoView();", add_to_cart_button)
            add_to_cart_button.click()

            # Aguarda até que o ícone do carrinho seja atualizado
            WebDriverWait(self.driver, 15).until(
                EC.text_to_be_present_in_element((By.CSS_SELECTOR, "span.shopping_cart_badge"), "1")
            )

            # Verifica se o número de itens no carrinho foi atualizado usando assertEquals
            cart_badge = self.driver.find_element("css selector", "span.shopping_cart_badge").text
            self.assertEqual(cart_badge, "1", "CT 2.2: Número de itens no carrinho não foi atualizado corretamente.")

        except TimeoutException as e:
            # Captura uma captura de tela e imprime a exceção
            timestamp = datetime.now().strftime("%Y%m%d%H%M%S")
            self.driver.save_screenshot(f"timeout_exception_{timestamp}.png")
            print(f"TimeoutException: {e}")

if __name__ == "__main__":
    unittest.main()
