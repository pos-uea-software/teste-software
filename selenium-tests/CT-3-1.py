import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from datetime import datetime

class TestAddMultipleItemsToCart(unittest.TestCase):

    def setUp(self):
        # Configuração do WebDriver (certifique-se de ter o WebDriver adequado instalado, como o ChromeDriver)
        self.driver = webdriver.Chrome()
        self.url = "https://www.saucedemo.com/"

    def tearDown(self):
        # Fecha o navegador após cada teste
        self.driver.quit()

    def test_add_multiple_items_to_cart(self):
        # Caso de Teste 3.1: Testar a adição de três itens diferentes ao carrinho.
        # Abre a página do SauceDemo
        self.driver.get(self.url)

        # Insere as credenciais válidas
        username = self.driver.find_element("css selector", "input#user-name")
        password = self.driver.find_element("css selector", "input#password")
        login_button = self.driver.find_element("css selector", "input#login-button")

        username.send_keys("standard_user")
        password.send_keys("secret_sauce")
        login_button.click()

        try:
            # Aguarda até que a lista de produtos esteja visível
            WebDriverWait(self.driver, 10).until(
                EC.visibility_of_element_located((By.CLASS_NAME, "inventory_list"))
            )

            # Lista de itens a serem adicionados ao carrinho (pode ser ajustada conforme necessário)
            items_to_add = [
                "Sauce Labs Backpack",
                "Sauce Labs Bolt T-Shirt",
                "Sauce Labs Onesie"
            ]

            # Adiciona os itens ao carrinho
            for item in items_to_add:
                item_name = item.split()[-1].lower()
                add_to_cart_button = self.driver.find_element("css selector", f"button.btn_inventory:contains('{item_name}')")
                add_to_cart_button.click()

            # Clica no ícone do carrinho
            cart_icon = self.driver.find_element("css selector", "a.shopping_cart_link")
            cart_icon.click()

            # Aguarda até que a página do carrinho seja carregada
            WebDriverWait(self.driver, 10).until(
                EC.title_contains("Your Cart")
            )

            # Verifica se os três itens estão no carrinho usando assertEquals
            cart_items = self.driver.find_elements("css selector", "div.inventory_item_name")
            expected_items = [item.strip() for item in items_to_add]

            for cart_item in cart_items:
                self.assertIn(cart_item.text.strip(), expected_items, "CT 3.1: Um ou mais itens não foram adicionados corretamente ao carrinho.")

            # Verifica se o total reflete os preços corretos usando assertEquals
            item_prices = self.driver.find_elements("css selector", "div.inventory_item_price")
            expected_prices = ["29.99", "15.99", "7.99"]

            total_price_element = self.driver.find_element("css selector", "div.summary_total_label")
            actual_total_price = total_price_element.find_element("xpath", "./following-sibling::div").text

            expected_total_price = "${:.2f}".format(sum(float(price) for price in expected_prices))

            self.assertEqual(actual_total_price, expected_total_price, "CT 3.1: O total no carrinho não reflete os preços corretos.")

        except TimeoutException as e:
            # Captura uma captura de tela e imprime a exceção
            timestamp = datetime.now().strftime("%Y%m%d%H%M%S")
            self.driver.save_screenshot(f"timeout_exception_{timestamp}.png")
            print(f"TimeoutException: {e}")

if __name__ == "__main__":
    unittest.main()
